# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{ config, pkgs, ... }:
{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware.nix
      ./modules.nix
    ];

    # Define your hostname.
  networking.hostName = "venus";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Set the keyboard layout.
  services.xserver.layout = "us";
  services.xserver.videoDrivers = [ "intel" "nouveau" ];
  #  services.syncthing.enable = true;
#  services.syncthing.group = "users";
#  services.syncthing.dataDir = "/home/hive";
#  services.syncthing.configDir = /home/hive/.config/syncthing;
#  services.syncthing.user = "hive";
  console.useXkbConfig = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users."hive" = {
    isNormalUser = true;
    description = "The hive";
    extraGroups = [ "wheel" "networkmanager" "dialout" "gamemode" ];
  };
  users.defaultUserShell = pkgs.fish;
  

  # Allow unfree packages
  environment.sessionVariables.NIXPKGS_ALLOW_UNFREE = "1";

    # List packages installed in system profile.
  environment.systemPackages = with pkgs; [
    mangohud
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

  programs.nix-data = {
    enable = true;
    systemconfig = "/etc/nixos/systems/x86_64-linux/venus/default.nix";
    flake = "/etc/nixos/flake.nix";
    flakearg = "venus";
  };
  programs.steam.enable = true;
  programs.gamemode.enable = true;
  programs.gamemode.settings = {
    general = {
		desiredgov="performance";
      renice=10;
	 };
	 gpu = {
		apply_gpu_optimisations="accept-responsibility";
		gpu_device=0;
    };
  };
  programs.gamemode.enableRenice = true;
  programs.fish.enable = true;
  hardware.opengl.enable = true;
  hardware.opengl.extraPackages = with pkgs; [ xorg.xf86videonouveau ];
  hardware.nvidia.modesetting.enable = true;
  hardware.opengl.driSupport = true;
  hardware.opengl.driSupport32Bit = true;
}
