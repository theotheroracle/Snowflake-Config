# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware.nix
      ./modules.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.binfmt.emulatedSystems = [ "aarch64-linux" "riscv64-linux" ];
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.luks.devices."luks-ece06fb8-70eb-4f4e-9efd-b035e23521c2".device = "/dev/disk/by-uuid/ece06fb8-70eb-4f4e-9efd-b035e23521c2";
  networking.hostName = "library"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.fprintd.enable = true;
  services.fwupd.enable = true;
  services.fwupd.extraRemotes = [
  "lvfs-testing"
];
  services.aria2.enable = true;
  services.aria2.rpcSecretFile = "/run/secrets/aria2c/aria2-rpc-token.txt";
  services.aria2.downloadDir = "/home/hive/Downloads";
  services.tailscale.enable = true;
  services.tailscale.useRoutingFeatures = "client";

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = with pkgs; [ brlaser gutenprint gutenprintBin ];

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  hardware.bluetooth.enable = true; # enables support for Bluetooth
  hardware.bluetooth.powerOnBoot = true; # powers up the default Bluetooth controller on boot
  security.rtkit.enable = true;
  security.pki.certificates = [ 
''
pegasi.gg.lan
=========
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUdAqA3SIiPCxy2XKSFx0hxEQsWnAwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCVVMxETAPBgNVBAgMCE5ldy1Zb3JrMRMwEQYDVQQKDApH
ZWVreSBHYXlzMQ4wDAYDVQQDDAVHZWVrczAeFw0yNDAzMTQxODUzNDFaFw0yOTAz
MTMxODUzNDFaMEUxCzAJBgNVBAYTAlVTMREwDwYDVQQIDAhOZXctWW9yazETMBEG
A1UECgwKR2Vla3kgR2F5czEOMAwGA1UEAwwFR2Vla3MwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCmIVDYhfkImjaKGr0I/ZYAxPACTUd539s6BzdvdZTg
127yHCWCDhmJfCc3csDUp/v3GI33xkPNcFmcQ/6t+C3fg7NVo+4sbNdl/KeZN+2p
NQA9o08J4N7LEl7EKgr7HS9CqF8IDOXSg6jmRoCyCmZvnMrCjJGOToM6SWacVY4c
06tJxtqzs/rV6mW1GtReZURbawwFs7hGI/jggavdQfeyyrQmrcCMiOLQg/vX7/TH
ANhKd5FZTH0uterOe11rBiS27aeJBt7VLio+0GgHKNvamErNhMGhfywQ1/Ojwamc
xnYLH2Mo8t3silRyJwzHfog5dGoew6o4oBQJZorfk0j5AgMBAAGjUzBRMB0GA1Ud
DgQWBBR1bEWHK/SDMegdeyZgzMAVurSL7zAfBgNVHSMEGDAWgBR1bEWHK/SDMegd
eyZgzMAVurSL7zAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBT
zynXPta1pWTigj31AULChiX9EdecYK+MEBZefdBbozeYFBxujG4qv/rSmCUNfCBx
qOa6aWGe6TKJERhPu2qRJ7iKp9Ys9gyxtM1qYpmdMzZemD07IySv+7wmEJZSPnfP
dd4O/otEFe2X1Kg73EeujAC7JwCBCMlQ4qGfPqYpWTRZyuruwXoMth6qNfiW1hAw
mvOsTX5Zt2uEfky0tbSO4Q3+SNzTJFAd5QMD9edmdGJE4ihGPiU8kWecg/xZFWD2
eFUt8icSLrAwVsXOWH322AO4ReaKtfFaJB68nD9GqAgceN/sxU/B1WH5m3U/arUH
4d/dkAcejjbuOhrhzs6H
-----END CERTIFICATE-----
''
  ];
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.hive = {
    isNormalUser = true;
    description = "Hive";
    extraGroups = [ "networkmanager" "wheel" "aria2" "dialout" ];
    packages = with pkgs; [
      kate
    #  thunderbird
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #  wget
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  programs.steam.enable = true;
  programs.firefox.nativeMessagingHosts.packages = [
pkgs.firefoxpwa
pkgs.browserpass
];
  programs.firefox.enable = true;
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = [ 
    pkgs.zmusic 
	 pkgs.SDL2 
	 pkgs.openal 
	 pkgs.fuse
    pkgs.spdlog 
	 pkgs.alsa-lib
	 pkgs.kdePackages.full
	 pkgs.e2fsprogs
	 pkgs.harfbuzz
	 pkgs.glib
	 pkgs.xorg.libX11
	 pkgs.xorg.libxcb
	 pkgs.fontconfig
	 pkgs.freetype
	 pkgs.expat
	 pkgs.curlFull
	 pkgs.kdePackages.qtbase
	 pkgs.kdePackages.qt5compat
	 pkgs.xorg.libSM
	 pkgs.xorg.libICE
  ];
  virtualisation.waydroid.enable = true;
  virtualisation.libvirtd.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 35915 ];
  networking.firewall.allowedUDPPorts = [ 35915 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
