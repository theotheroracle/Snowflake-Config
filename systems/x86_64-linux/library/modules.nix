{ inputs, config, pkgs, lib, system, ... }:
{
  services.flatpak.enable = true;
  services.pipewire.jack.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_zen;
  modules.packagemanagers.appimage.enable = true;
}
