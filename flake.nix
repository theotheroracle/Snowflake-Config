{
  description = "among us";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    nix-data = {
      url = "github:snowflakelinux/nix-data";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    snowfall-lib = {
      url = "github:snowfallorg/lib";
      inputs.nixpkgs.follows = "nixpkgs";
		inputs.flake-utils-plus.url = "github:gytis-ivaskevicius/flake-utils-plus?ref=master";
    };

    home-manager = {
		url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
	 };
	nix-software-center.url = "github:snowfallorg/nix-software-center";
	nixos-conf-editor.url = "github:snowfallorg/nixos-conf-editor";

	 nixos-hardware.url = "github:theotheroracle/nixos-hardware/master";
    snowflakeos-modules.url = "github:snowflakelinux/snowflakeos-modules";
  };

  outputs = inputs:
    inputs.snowfall-lib.mkFlake {
      inherit inputs;
      src = ./.;

      channels-config.allowUnfree = true;
      systems.modules.nixos = with inputs; [
			nix-data.nixosModules.nix-data
			snowflakeos-modules.nixosModules.efiboot
			snowflakeos-modules.nixosModules.kernel
			snowflakeos-modules.nixosModules.packagemanagers
			snowflakeos-modules.nixosModules.pipewire
			snowflakeos-modules.nixosModules.printing
      ];
	};
}
