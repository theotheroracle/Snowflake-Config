{config, pkgs, inputs, ... }:
{
	environment.systemPackages = with pkgs; [
		inputs.nixos-conf-editor.packages.${system}.nixos-conf-editor
		inputs.nix-software-center.packages.${system}.nix-software-center
	];
}
