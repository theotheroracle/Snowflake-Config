{ ... }:
{
  boot.loader.systemd-boot.enable = true;
  boot.consoleLogLevel = 3;
  boot.kernelParams = [ "quiet" ];
  boot.loader.timeout = 1;
  boot.loader.systemd-boot.configurationLimit = 5;
  boot.initrd.systemd.enable = true;
  boot.plymouth.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
}
