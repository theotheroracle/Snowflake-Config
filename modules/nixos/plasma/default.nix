{ pkgs, ... }:
{
  # Enable the KDE Plasma Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.displayManager.sddm.wayland.enable = true;
  services.displayManager.sddm.wayland.compositor = "kwin";
  services.displayManager.sddm.enable = true;
  services.xserver.displayManager.sddm.wayland.enable = true;
  services.xserver.displayManager.defaultSession = "plasma";
  services.xserver.desktopManager.plasma6.enable = true;
  environment.systemPackages = [
    pkgs.kdePackages.kmail
    pkgs.kdePackages.kdepim-addons
    pkgs.kdePackages.tokodon
    pkgs.kdePackages.qt6ct
    pkgs.kdePackages.qt6gtk2
    pkgs.kdePackages.audiotube
    pkgs.kdePackages.angelfish
    pkgs.kdePackages.akonadi
    pkgs.kdePackages.accounts-qt
#    pkgs.kdePackages.kalzium
    pkgs.kdePackages.kaddressbook
    pkgs.kdePackages.kaccounts-providers
	 pkgs.kdePackages.discover
    pkgs.kdePackages.kaccounts-integration
    pkgs.kdePackages.flatpak-kcm
    pkgs.kdePackages.filelight
	 pkgs.kdePackages.partitionmanager
    pkgs.kdePackages.falkon
    pkgs.kdePackages.kdeconnect-kde
    pkgs.kdePackages.kde-gtk-config
    pkgs.kdePackages.merkuro
    pkgs.kdePackages.akonadi-import-wizard
    pkgs.kdePackages.tokodon
	 pkgs.kdePackages.kmail-account-wizard
	 pkgs.kdePackages.kate
    pkgs.krita
	 pkgs.libsForQt5.bismuth
	 pkgs.wayland-utils
	 pkgs.vulkan-tools
	 pkgs.glxinfo
	 pkgs.clinfo
	 pkgs.pciutils
	 pkgs.aha
];
}
