{ pkgs, lib }: with builtins;
rec
{
	mkSettings = s:
		concatStringsSep
			"\n"
			(attrValues
				(mapAttrs
					(k: v:
						if typeOf v == "bool" then
							if v then "set ${k}" else "set no${k}"
						else
							"set ${k}=${toString v}")
					s
				)
			);
	boolToString = b: if b then "true" else "false";
	toLuaTable = as:
		"{" +
		( concatStringsSep
			", "
			( filter
				(attr: !isNull attr)
				( attrValues
					( mapAttrs
						(k: v: if v then "${k}=true" else null)
						as
					)
				)
			)
		) +
		"}";
	mkMapping = {
		mode,
		binding,
		command,
		nowait?false,
		silent?false,
		unique?false,
		expr?false
	}:
		"vim.api.nvim_set_keymap(${toJSON mode}, ${toJSON binding}, ${toJSON command}, ${toLuaTable {inherit nowait silent unique expr;}})";
	mkMappings =
		m:
			"lua << END-OF-KEYBINDS\n" +
			(concatStringsSep "\n" (map mkMapping m)) +
			"\nEND-OF-KEYBINDS";
	mkRuntimes = rs:
		if rs == [] then "" else
		"set runtimepath+=" +
		( concatStringsSep
			","
			(map (rt: "${rt}") rs)
		);
	mkVimHeader = h: "\"\"\"\"\"\"\"\" ${h} \"\"\"\"\"\"\"\"\"";
	mkVimSection = section:
		(concatStringsSep "\n\n\n"
			(filter
				(v: !isNull v)
				(attrValues
					(mapAttrs
						(k: v: if "" == v then null else (mkVimHeader k) + "\n" + v)
						section
					)
				)
			)
		);
	mkVimSections = sections: concatStringsSep "\n\n\n" (map mkVimSection sections);
	mkConfig = { settings?{}, mappings?[], runtimes?[], extra?"" }:
		mkVimSections [
			{"ADDITIONAL RUNTIME DIRS" = mkRuntimes runtimes;}
			{"SETTINGS" = mkSettings settings;}
			{"KEY BINGINGS" = mkMappings mappings;}
			{"MANUAL SETTINGS" = extra;}
		];
}
