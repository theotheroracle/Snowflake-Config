{ config, pkgs, lib, ... }: 
with builtins; 
with import ./modules/neovim-config.nix { inherit pkgs lib; };
{
	programs.home-manager.enable = true;
	home.username = "hive";
	home.homeDirectory = "/home/hive";
	home.packages = with pkgs; [
		appimage-run
		tdesktop
		goverlay
		lutris
		simplex-chat-desktop
		qalculate-qt
		#android-studio
		tree
		tmux
		steam-run
		usbimager
		signal-desktop
		bottles
		#logseq
		prusa-slicer
		mosh
		pods
		tuba
		bat
		diceware
		superTuxKart
		prismlauncher
		birdfont
		ungoogled-chromium
		darktable
		nheko
		mgba
		blender
		srain
		onlyoffice-bin
		dconf2nix
		inkscape
		minetest
		vscodium
		flare-signal
		aria
		pwgen
		josm
		xdg-ninja
		godot_4
		dino
		jq
		lapce
		mangohud
		nix-index
		gimp
#		lollypop
		skim
		nix-output-monitor
		#picard
		armcord
		ripgrep
		tmate
		comic-mono
		comfortaa
		btop
		warp
		virt-manager
		mpv
		wl-clipboard
		adw-gtk3
		(writeShellScriptBin "strings" ''${busybox}/bin/strings "$@"'')
	] ++ ( with pkgs.gnome;  [
		gnome-tweaks
		gnome-disk-utility
		dconf-editor
		gnome-boxes
	] ++ ( with pkgs.gnomeExtensions; [
		night-theme-switcher
		rounded-window-corners
		appindicator
		gsconnect
		pano
		user-avatar-in-quick-settings
		gamemode
	]));

	home.file = { # If managing individual files is needed
	};

	home.sessionVariables = {
		# fish config
		fish_greeting = "✨ heya $USER, welcome to $(hostnamectl --pretty) ~";

		SDL_GAMECONTROLLERCONFIG = "0300db38550900001472000011010000,NVIDIA Controller v01.04 - HiveMap,platform:Linux,a:b0,b:b1,x:b2,y:b3,back:b7,guide:b4,start:b9,leftstick:b10,rightstick:b11,leftshoulder:b5,rightshoulder:b6,dpup:h0.1,dpdown:h0.4,dpleft:h0.8,dpright:h0.2,leftx:a0,lefty:a1,rightx:a2,righty:a5,lefttrigger:a3,righttrigger:a4,crc:38db,";

		EDITOR = "nvim";
		
		NIXPKGS_ALLOW_UNFREE = 1;

#		GAMEMODERUNEXEC = "nvidia-offload";
	};
	systemd.user.targets.tray = {
		Unit = {
			Description = "Home Manager System Tray";
			Requires = [ "graphical-session-pre.target" ];
		};
	};
	# configure dconf
	# use dconf watch / to monitor for changes
	# use dconf dump /path/to/keys | dconf2nix to generate automatically
#	dconf.settings = with lib.hm.gvariant; {
#		"org/gnome/shell" = {
#			disable-user-extensions = false;
#			disable-extension-version-validation = true;
#			enabled-extensions = [
#				"rounded-window-corners@yilozt"
#				"gsconnect@andyholmes.github.io"
#				"pano@elhan.io"
#				"quick-settings-avatar@d-go"
#				"appindicatorsupport@rgcjonas.gmail.com"
#				"gamemode@christian.kellner.me"
#			];
#			disabled-extensions = [];
#			favorite-apps = [
#				"firefox.desktop"
#				"org.gnome.Nautilus.desktop"
#				"org.gnome.Software.desktop"
#			];
#		};
#		"org/gnome/shell/extensions/rounded-window-corners" = {
#			custom-rounded-corner-settings = [(mkDictionaryEntry [
#				"org.wezfurlong.wezterm" (mkVariant [
#					(mkDictionaryEntry [
#						"padding" (mkVariant [
#							(mkDictionaryEntry [
#								"left" (mkVariant (mkUint32 13))
#							])
#							(mkDictionaryEntry [
#								"right" (mkVariant (mkUint32 14))
#							])
#							(mkDictionaryEntry [
#								"top" (mkVariant (mkUint32 29))
#							])
#							(mkDictionaryEntry [
#								"bottom" (mkVariant (mkUint32 13))
#							])
#						])
#					])
#					(mkDictionaryEntry [
#						"keep_rounded_corners" (mkVariant [
#							(mkDictionaryEntry [
#								"maximized" (mkVariant false)
#							])
#							(mkDictionaryEntry [
#								"fullscreen" (mkVariant false)
#							])
#						])
#					])
#					(mkDictionaryEntry [
#						"border_radius" (mkVariant (mkUint32 13))
#					])
#					(mkDictionaryEntry [
#						"smoothing" (mkVariant (mkUint32 0))
#					])
#					(mkDictionaryEntry [
#						"enabled" (mkVariant true)
#					])
#				])
#			])];
#		};
#		"org/gnome/mutter".center-new-windows = true;
#		"org/gnome/desktop/sound".allow-volume-above-100-percent = true;
#		"org/gnome/settings-daemon/plugins/media-keys".custom-keybindings = [
#			"/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
#		];
#		"org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
#			binding = "<Super>t";
#			command = "wezterm";
#			name = "Wezterm";
#		};
#	};

	# configure programs
	programs.direnv = {
		enable = true;
	};
	programs.zoxide = {
		enable = true;
		enableFishIntegration = true;
		options = [ "--cmd cd" ];
	};
	programs.git = {
		enable = true;
		lfs.enable = true;
		delta = {
			enable = true;
			options = {
				decorations = {
					commit-decoration-style = "bold yellow box ul";
					file-decoration-style = "none";
					file-style = "bold yellow ul";
					hunk-header-decoration-style = "cyan box ul";
				};
				features = "side-by-side line-numbers decorations";
			};
		};
		signing = {
			key = "DD901BA40FBBB127";
			signByDefault = true;
		};
		userEmail = "theotheroracle@disroot.org";
		userName = "Bit Borealis";
		extraConfig = {
			init.defaultBranch = "main";
		};
	};
	programs.neovim = {
		enable = true;
		defaultEditor = true;
		viAlias = true;
		vimAlias = true;
		vimdiffAlias = true;
		withNodeJs = true;

		plugins = with pkgs.vimPlugins; [
			editorconfig-nvim
			vim-airline
			vim-airline-themes
			lsp_signature-nvim
			nvim-colorizer-lua
			nnn-vim
			nvim-surround
			nvim-treesitter.withAllGrammars
			nvim-lspconfig
			rainbow-delimiters-nvim
		];
		extraConfig = mkConfig {
			settings = {
				number = true;
				linebreak = true;
				showbreak = "+>";
				textwidth = 80;
				showmatch = true;

				hlsearch = true;
				smartcase = true;
				ignorecase = true;

				autoindent = true;
				expandtab = false;
				smartindent = true;
				smarttab = true;
				tabstop = 3;
				shiftwidth = 3;

				display = "lastline";
				formatoptions = "jt";
				ruler = true;
				colorcolumn = 80;
				termguicolors = true;
			};
		};
		coc = {
			enable = true;
		};
	};
	programs.fish = {
		enable = true;
		shellAbbrs = {
			e = "edit";
			nvim = "edit";
			nxs = "nix search nixpkgs";
			nxr = "nix run nixpkgs#";
			nxh = "nix shell nixpkgs#";
			nxc = "sudo nix-collect-garbage -d";
		};
		shellAliases = {
			edit = "$EDITOR";
			csk = "cd (sk -e)";
			nxu = ''sudo sh -c "nixos-rebuild boot --flake /home/hive/.config/saturn --upgrade --log-format internal-json -v |& nom --json"'';
			nxw = ''sudo sh -c "nixos-rebuild switch --flake /home/hive/.config/saturn --log-format internal-json -v |& nom --json"'';
			aria = "aria2c -c -k1M -x10";
			dw = "diceware -n 6 -d -";
			pw = "pwgen -By";
			p = "ping -s 128 -O -c 100 -A -D 1.1.1.1";
			pf = "sudo ping -f  -c 100 -A 1.1.1.1";
		};
		functions = {
			hme = ''
set -l hm_before_commit $PWD
cd ~/.config/saturn/
git pull
cd $hm_before_commit
edit ~/.config/saturn/modules/home/core/default.nix
'';
			hmu = ''
set -l hm_before_commit $PWD
cd ~/.config/saturn/
gacp
cd $hm_before_commit
'';
			gacp = ''
git diff
set gacp_read $(read -P 'continue ? [Y/n] ')
set gacp_n n
if [ "$gacp_read" != $gacp_n ]
	git add .
	git commit
	git push
else
	echo "cancelled ."
end
'';

			ng = ''
set NG ~/.ssh/ng/
if test \( -e $NG/0 \) -a \( -e $NG/1 \)
	echo (shuf -n 1 $NG/0) (shuf -n 1 $NG/1)
else 
	echo missing NG files
end
'';
			fish_prompt = ''
set -l last_status $status
prompt_login
echo -n ' : '
set_color $fish_color_cwd
echo -n (prompt_pwd)
set_color normal
fish_vcs_prompt '|%s'
echo " ඞ"
if not test $last_status -eq 0
    set_color $fish_color_error
end
echo -n "» "
set_color normal
'';
		};
		interactiveShellInit = ''
fish_add_path ~/.local/bin
fish_add_path ~/.local/bin/scripts
fish_add_path ~/.local/share/cargo/bin
fish_add_path ~/.local/share/flatpak/exports/bin
fish_add_path /var/lib/flatpak/exports/bin
'';
	};
	programs.password-store = {
		enable = true;
		package = pkgs.pass.withExtensions (exts: [ exts.pass-otp ]);
		settings = { 
			PASSWORD_STORE_DIR = "${config.xdg.dataHome}/pass";
			PASSWORD_STORE_CLIP_TIME = "60";
		};
	};
	programs.browserpass.enable = true;
	programs.gpg.enable = true;
	services.syncthing = {
		enable = true;
		tray.enable = true;
	};
	services.gpg-agent = {
		enable = true;
		enableFishIntegration = true;
		pinentryPackage = pkgs.pinentry-qt;
	};
	services.ssh-agent.enable = true;


	# themeing
#	qt = {
#		enable = true;
#		platformTheme = "gnome";
#		style.name = "adwaita";
#	};
}
